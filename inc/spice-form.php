<span id="spice_popup_login_id">
<form id="spice_popup_login" class="spice-popup-login-form" action="login" method="post">
    <?php
    if(!empty(get_theme_mod('spice_popup_login_heading','New to site? <a id="spice_signup" href="">Create an Account</a>'))):?>
    <h3><?php echo get_theme_mod('spice_popup_login_heading','New to site? <a id="spice_signup" href="">Create an Account</a>');?></h3>
    <hr><?php endif;?>
    <h2><?php echo get_theme_mod('spice_popup_login_title',esc_html__('Login','spice-popup-login'));?></h2>
    <p class="status"></p>
    <?php wp_nonce_field('spice-ajax-login-nonce','spice-ajax-security');?>
    <label for="username"><?php echo get_theme_mod('spice_popup_login_username',esc_html__('Username','spice-popup-login'));?></label>
    <input id="username" type="text" class="required" name="username">
    <label for="password"><?php echo get_theme_mod('spice_popup_login_password',esc_html__('Password','spice-popup-login'));?></label>
    <input id="password" type="password" class="required" name="password">
    <?php 
    if(!empty(get_theme_mod('spice_popup_login_password_lost',esc_html__('Lost password?','spice-popup-login')))):?>
    <a class="spice-popup-lostpassword" href="<?php echo wp_lostpassword_url(); ?>"><?php echo get_theme_mod('spice_popup_login_password_lost',esc_html__('Lost password?','spice-popup-login'));?></a><?php endif;?>
    <input class="spice_popup_submit_button" type="submit" value="<?php echo get_theme_mod('spice_popup_login_btn',esc_html__('Login','spice-popup-login'));?>">
	<a class="close" href="">x</a>
</form>
<form id="spice_popup_register" class="spice-popup-login-form" action="register" method="post">
    <?php if(!empty(get_theme_mod('spice_popup_register_heading','Already have an account? <a id="spice_login" href="" style="outline: none;">Login</a>'))):?>
	<h3><?php echo get_theme_mod('spice_popup_register_heading','Already have an account? <a id="spice_login" href="" style="outline: none;">Login</a>');?></h3>
    <hr><?php endif;?>
    <h2><?php echo get_theme_mod('spice_popup_register_title',esc_html__('Signup','spice-popup-login'));?></h2>
    <p class="status"></p>
    <?php wp_nonce_field('spice-ajax-register-nonce','spice-ajax-reg-security');?>
    <label for="signonname"><?php echo get_theme_mod('spice_popup_register_username',esc_html__('Username','spice-popup-login'));?></label>
    <input id="signonname" type="text" name="signonname" class="required">
    <label for="email"><?php echo get_theme_mod('spice_popup_register_mail',esc_html__('Email','spice-popup-login'));?></label>
    <input id="email" type="text" class="required email" name="email">
    <label for="signonpassword"><?php echo get_theme_mod('spice_popup_register_password',esc_html__('Password','spice-popup-login')); ?></label>
    <input id="signonpassword" type="password" class="required" name="signonpassword">
    <label for="password2"><?php echo get_theme_mod('spice_popup_register_confirm_password',esc_html__('Confirm Password','spice-popup-login'));?></label>
    <input type="password" id="password2" class="required" name="password2">
    <input class="spice_popup_submit_button" type="submit" value="<?php echo get_theme_mod('spice_popup_register_btn',esc_html__('Signup?','spice-popup-login')); ?>">
    <a class="close" href="">x</a>
</form>
</span>