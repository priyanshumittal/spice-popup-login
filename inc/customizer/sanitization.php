<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Text sanitization callback
function spice_popup_login_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}


// Checkbox sanitization callbac
function spice_popup_login_sanitize_checkbox($checked) {
    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );
}


// Typography callback
function spice_popup_login_typo_callback($control) {
    if (false == $control->manager->get_setting('spice_popup_login_typo')->value()) {
        return false;
    } else {
        return true;
    }
}


// Color callback
function spice_popup_login_color_callback($control) {
    if (false == $control->manager->get_setting('enable_spice_popup_login_clr')->value()) {
        return false;
    } else {
        return true;
    }
}