<?php
// Adding customizer settings
function spice_popup_login_customizer_controls( $wp_customize )
{
   $spice_popup_login_font_size = array();
    for($i=10; $i<=100; $i++) {
      $spice_popup_login_font_size[$i] = $i;
    }
    $spice_popup_login_line_height = array('inherit'=>'Default');
    for($i=0; $i<=100; $i++) {
        $spice_popup_login_line_height[$i] = $i;
    }
    $spice_popup_login_font_style = array('normal'=>'Normal','italic'=>'Italic');
    $spice_popup_login_text_transform = array('default'=>'Default','capitalize'=>'Capitalize','lowercase'=>'Lowercase','Uppercase'=>'Uppercase');
    $spice_popup_login_font_weight = array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900');
    $spice_popup_login_font_family = spice_popup_login_typo_fonts();
    

    /*===================================================================================================================
    *  Spice Popup Login *
    * ====================================================================================================================== */
    
    $wp_customize->add_panel('spice_popup_login',
        array(
            'priority'   => 155,
            'capability' => 'edit_theme_options',
            'title'      => esc_html__('Spice Popup Login','spice-popup-login')
        )
    );

    
    /*===================================================================================================================
    *  Spice Popup Login General Setting *
    * ====================================================================================================================== */
    
    $wp_customize->add_section( 'spice_popup_login_customizer' , array(
        'title'    => esc_html__('General Settings', 'spice-popup-login'),
        'priority' => 1,
        'panel'    => 'spice_popup_login',
        )
    );
    

    // Login Text
    $wp_customize->add_setting('spice_popup_login_text',
        array(
            'default'           =>  esc_html__('Sign in / Join', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_text', 
        array(
            'label'    =>  esc_html__('Login Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_text',
            'type'     =>  'text',
            'priority' =>  1,
        )
    );


   // Logout Text
    $wp_customize->add_setting('spice_popup_login_logout_text',
        array(
            'default'           =>  esc_html__('Logout', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_logout_text', 
        array(
            'label'    =>  esc_html__('Logout Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_logout_text',
            'type'     =>  'text',
            'priority' =>  2,
        )
    );

    // Login Text
    class Spice_Popup_Login_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('LOGIN FORM', 'spice-popup-login' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('spice_popup_login_form',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Customize_Control($wp_customize, 'spice_popup_login_form', 
        array(
            'section'           =>  'spice_popup_login_customizer',
            'setting'           =>  'spice_popup_login_form',
            'priority' =>  3,
        )
    ));

    // LOGIN FORM HEADING
    $wp_customize->add_setting('spice_popup_login_heading',
        array(
            'default'           =>  esc_html__('New to site? <a id="spice_signup" href="">Create an Account</a>', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_heading', 
        array(
            'label'    =>  esc_html__('Heading','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_heading',
            'type'     =>  'textarea',
            'priority' =>  4,
        )
    );


    // LOGIN FORM TITLE
    $wp_customize->add_setting('spice_popup_login_title',
        array(
            'default'           =>  esc_html__('Login', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_title', 
        array(
            'label'    =>  esc_html__('Title','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_title',
            'type'     =>  'text',
            'priority' =>  5,
        )
    );

    // Login Form Username
    $wp_customize->add_setting('spice_popup_login_username',
        array(
            'default'           =>  esc_html__('Username', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_username', 
        array(
            'label'    =>  esc_html__('Username Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_username',
            'type'     =>  'text',
            'priority' =>  6,
        )
    );

    // Login Form Password
    $wp_customize->add_setting('spice_popup_login_password',
        array(
            'default'           =>  esc_html__('Password', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_password', 
        array(
            'label'    =>  esc_html__('Password Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_password',
            'type'     =>  'text',
            'priority' =>  7,
        )
    );


    // Login Form Forget Password
    $wp_customize->add_setting('spice_popup_login_password_lost',
        array(
            'default'           =>  esc_html__('Lost password?', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_password_lost', 
        array(
            'label'    =>  esc_html__('Forget Password Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_password_lost',
            'type'     =>  'text',
            'priority' =>  8,
        )
    ); 

    
    // Login Button
    $wp_customize->add_setting('spice_popup_login_btn',
        array(
            'default'           =>  esc_html__('Login', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn', 
        array(
            'label'    =>  esc_html__('Login Button Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_login_btn',
            'type'     =>  'text',
            'priority' =>  9,
        )
    );


    // Register Form
    class Spice_Popup_Register_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('REGISTER FORM', 'spice-popup-login' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('spice_popup_register_form',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Register_Customize_Control($wp_customize, 'spice_popup_register_form', 
        array(
            'section'           =>  'spice_popup_login_customizer',
            'setting'           =>  'spice_popup_register_form',
            'priority' =>  10,
        )
    )); 


    // Register Form Heading
    $wp_customize->add_setting('spice_popup_register_heading',
        array(
            'default'           =>  esc_html__('Already have an account? <a id="spice_login" href="" style="outline: none;">Login</a>', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_heading', 
        array(
            'label'    =>  esc_html__('Heading','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_heading',
            'type'     =>  'textarea',
            'priority' =>  11,
        )
    );


    // Register Form Title
    $wp_customize->add_setting('spice_popup_register_title',
        array(
            'default'           =>  esc_html__('Signup', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_title', 
        array(
            'label'    =>  esc_html__('Title','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_title',
            'type'     =>  'text',
            'priority' =>  12,
        )
    );

    // Register Form Username
    $wp_customize->add_setting('spice_popup_register_username',
        array(
            'default'           =>  esc_html__('Username', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_username', 
        array(
            'label'    =>  esc_html__('Username Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_username',
            'type'     =>  'text',
            'priority' =>  13,
        )
    );   

   // Register Form Main
    $wp_customize->add_setting('spice_popup_register_mail',
        array(
            'default'           =>  esc_html__('Email', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_mail', 
        array(
            'label'    =>  esc_html__('Email Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_mail',
            'type'     =>  'text',
            'priority' =>  14,
        )
    ); 

    // Register Form Password
    $wp_customize->add_setting('spice_popup_register_password',
        array(
            'default'           =>  esc_html__('Password', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_password', 
        array(
            'label'    =>  esc_html__('Password Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_password',
            'type'     =>  'text',
            'priority' =>  15,
        )
    );        


    // Register Form Confirm Password
    $wp_customize->add_setting('spice_popup_register_confirm_password',
        array(
            'default'           =>  esc_html__('Confirm Password', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_confirm_password', 
        array(
            'label'    =>  esc_html__('Confirm Password Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_confirm_password',
            'type'     =>  'text',
            'priority' =>  16,
        )
    ); 


    // Register Form Signup Button
    $wp_customize->add_setting('spice_popup_register_btn',
        array(
            'default'           =>  esc_html__('Signup', 'spice-popup-login' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_register_btn', 
        array(
            'label'    =>  esc_html__('Signup/Register Text','spice-popup-login' ),
            'section'  =>  'spice_popup_login_customizer',
            'setting'  =>  'spice_popup_register_btn',
            'type'     =>  'text',
            'priority' =>  17,
        )
    );    


    /*===================================================================================================================
    *   Spice Popup Login Typography Setting *
    *===================================================================================================================*/
    
    $wp_customize->add_section( 'spice_popup_login_typo_section' , array(
        'title'   => esc_html__('Typography Settings', 'spice-popup-login'),
        'priority'=> 2,
        'panel'   => 'spice_popup_login',
        ) 
    );

    $wp_customize->add_setting('spice_popup_login_typo',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Toggle_Control( $wp_customize, 'spice_popup_login_typo',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice_popup_login'  ),
            'section'   =>  'spice_popup_login_typo_section',
            'setting'   =>  'spice_popup_login_typo',
            'type'      =>  'toggle'
        )
    ));


    Class Spice_Popup_Login_Heading_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Sign in  / Logout', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_heading_typo',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Heading_Customize_Control($wp_customize, 'spice_popup_login_heading_typo', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_heading_typo'
            )
        )
    );


    //Panel Heading Font Family
    $wp_customize->add_setting('spice_popup_login_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_fontfamily', array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
    ));


    //Panel Heading Font Size
    $wp_customize->add_setting('spice_popup_login_fontsize',
        array(
            'default'           =>  14,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
            )
    );
    $wp_customize->add_control('spice_popup_login_fontsize', array(
            'label'   => esc_html__('Font Size (px)','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_fontsize',
            'type'    => 'select',
            'choices' =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
    ));


    //Panel Heading Line Height
    $wp_customize->add_setting('spice_popup_login_lheight',
        array(
            'default'           => 'inherit',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_lheight', 
        array(
            'label'   =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_lheight',
            'type'    => 'select',
            'choices' => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Panel Heading Font Weight
    $wp_customize->add_setting('spice_popup_login_fontweight',
        array(
            'default'           =>  600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_fontweight', 
        array(
            'label'   =>  esc_html__('Font Weight','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_fontweight',
            'type'    => 'select',
            'choices' =>  $spice_popup_login_font_weight,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    //Panel Heading Font Style
    $wp_customize->add_setting('spice_popup_login_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_fontstyle', 
        array(
            'label'   => esc_html__('Font Style','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_fontstyle',
            'type'    => 'select',
            'choices' => $spice_popup_login_font_style,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    //Panel Heading Text Transform
    $wp_customize->add_setting('spice_popup_login_transform',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_transform', 
        array(
            'label'   => esc_html__('Text Transform','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_transform',
            'type'    => 'select',
            'choices'=>   $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    Class Spice_Popup_Login_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Login / Register Form', 'spice-popup-login' ); ?></h3>  
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_title_heading',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Title_Customize_Control($wp_customize, 'spice_popup_login_title_heading', 
        array(
               'active_callback'    =>  'spice_popup_login_typo_callback',
               'section'            =>  'spice_popup_login_typo_section',
               'setting'            =>  'spice_popup_login_title_heading'
            )
        )
    );


    Class Spice_Popup_Login_Heading_Typo_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading', 'spice-popup-login' ); ?></h3>  
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_headings_typo',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Heading_Typo_Customize_Control($wp_customize, 'spice_popup_login_headings_typo', 
        array(
               'active_callback'    =>  'spice_popup_login_typo_callback',
               'section'            =>  'spice_popup_login_typo_section',
               'setting'            =>  'spice_popup_login_headings_typo'
            )
        )
    );


    //Form Heading Font Family
    $wp_customize->add_setting('spice_popup_login_headings_fontfamily',
        array(
        'default'           =>  'Poppins',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_fontfamily', 
        array(
            'label'           =>  esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_headings_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Form Heading Font Size
    $wp_customize->add_setting('spice_popup_login_headings_fontsize',
        array(
        'default'           =>  18,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_fontsize', 
        array(
            'label'           => esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_headings_fontsize',
            'type'            => 'select',
            'choices'         => $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Form Heading Line Height
    $wp_customize->add_setting('spice_popup_login_headings_lheight',
        array(
            'default'           =>   18,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_lheight',
        array(
            'label'           =>   esc_html__('Line Height (px)','spice-popup-login'),
            'section'         =>  'spice_popup_login_typo_section',
            'setting'         =>  'spice_popup_login_headings_lheight',
            'type'            =>  'select',
            'choices'         =>  $spice_popup_login_line_height,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    //Form Heading Font Weight
    $wp_customize->add_setting('spice_popup_login_headings_fontweight',
        array(
            'default'           =>  500,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_fontweight', 
        array(
            'label'           => esc_html__('Font Weight','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_headings_fontweight',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_weight,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    //Form Heading Font Style
    $wp_customize->add_setting('spice_popup_login_headings_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_fontstyle', 
        array(
            'label'           => esc_html__('Font Style','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_headings_fontstyle',
            'type'            => 'select',
            'choices'         => $spice_popup_login_font_style,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    //Form Heading Text Transform
    $wp_customize->add_setting('spice_popup_login_headings_transform',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_headings_transform', array(
            'label'   =>  esc_html__('Text Transform','spice-popup-login'),
            'section' => 'spice_popup_login_typo_section',
            'setting' => 'spice_popup_login_headings_transform',
            'type'    => 'select',
            'choices'=>   $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    Class Spice_Popup_Login_Content_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Form Title', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_content_heading',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Content_Customize_Control($wp_customize, 'spice_popup_login_content_heading', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_content_heading'
            )
    ));


    //Title Font Family
    $wp_customize->add_setting('spice_popup_login_title_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_fontfamily', 
        array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_title_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );    


    //Title Font Size
    $wp_customize->add_setting('spice_popup_login_title_fontsize',
        array(
        'default'           =>  27,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_title_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Title Line Height
    $wp_customize->add_setting('spice_popup_login_title_lheight',
        array(
            'default'           =>  30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_title_lheight',
            'type'            => 'select',
            'choices'         => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Title Font Weight
    $wp_customize->add_setting('spice_popup_login_title_fontweight',
        array(
            'default'           =>  600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-popup-login'),
        'section'         => 'spice_popup_login_typo_section',
        'setting'         => 'spice_popup_login_title_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_popup_login_font_weight,
        'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    //Title Font Style
    $wp_customize->add_setting('spice_popup_login_title_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-popup-login'),
        'section'        => 'spice_popup_login_typo_section',
        'setting'        => 'spice_popup_login_title_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_popup_login_font_style,
        'active_callback'=> 'spice_popup_login_typo_callback',
        )
    );


    //Title Text Transform
    $wp_customize->add_setting('spice_popup_login_title_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_title_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_title_transform',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );




    Class Spice_Popup_Login_Label_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Label', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_label',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Label_Customize_Control($wp_customize, 'spice_popup_login_label', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_label'
            )
    ));


    //Label Font Family
    $wp_customize->add_setting('spice_popup_login_label_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_fontfamily', 
        array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_label_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );    


    //Label Font Size
    $wp_customize->add_setting('spice_popup_login_label_fontsize',
        array(
        'default'           =>  14,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_label_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Label Line Height
    $wp_customize->add_setting('spice_popup_login_label_lheight',
        array(
            'default'           =>  22,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_label_lheight',
            'type'            => 'select',
            'choices'         => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Label Font Weight
    $wp_customize->add_setting('spice_popup_login_label_fontweight',
        array(
            'default'           =>  400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-popup-login'),
        'section'         => 'spice_popup_login_typo_section',
        'setting'         => 'spice_popup_login_label_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_popup_login_font_weight,
        'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    //Label Font Style
    $wp_customize->add_setting('spice_popup_login_label_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-popup-login'),
        'section'        => 'spice_popup_login_typo_section',
        'setting'        => 'spice_popup_login_label_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_popup_login_font_style,
        'active_callback'=> 'spice_popup_login_typo_callback',
        )
    );


    //Label Text Transform
    $wp_customize->add_setting('spice_popup_login_label_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_label_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_label_transform',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    ); 



    Class Spice_Popup_Login_Txt_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Text / Forget Password', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_txt',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Txt_Customize_Control($wp_customize, 'spice_popup_login_txt', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_txt'
            )
    ));


    //Txt Font Family
    $wp_customize->add_setting('spice_popup_login_txt_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_fontfamily', 
        array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_txt_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );    


    //Txt Font Size
    $wp_customize->add_setting('spice_popup_login_txt_fontsize',
        array(
        'default'           =>  11,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_txt_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Txt Line Height
    $wp_customize->add_setting('spice_popup_login_txt_lheight',
        array(
            'default'           =>  18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_txt_lheight',
            'type'            => 'select',
            'choices'         => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Txt Font Weight
    $wp_customize->add_setting('spice_popup_login_txt_fontweight',
        array(
            'default'           =>  400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-popup-login'),
        'section'         => 'spice_popup_login_typo_section',
        'setting'         => 'spice_popup_login_txt_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_popup_login_font_weight,
        'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    //Txt Font Style
    $wp_customize->add_setting('spice_popup_login_txt_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-popup-login'),
        'section'        => 'spice_popup_login_typo_section',
        'setting'        => 'spice_popup_login_txt_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_popup_login_font_style,
        'active_callback'=> 'spice_popup_login_typo_callback',
        )
    );


    //Txt Text Transform
    $wp_customize->add_setting('spice_popup_login_txt_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_txt_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_txt_transform',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );




    Class Spice_Popup_Login_Btn_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Button', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_btn_typo',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Btn_Customize_Control($wp_customize, 'spice_popup_login_btn_typo', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_btn_typo'
            )
    ));


    //Btn Font Family
    $wp_customize->add_setting('spice_popup_login_btn_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_fontfamily', 
        array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );    


    //Btn Font Size
    $wp_customize->add_setting('spice_popup_login_btn_fontsize',
        array(
        'default'           =>  13,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Btn Line Height
    $wp_customize->add_setting('spice_popup_login_btn_lheight',
        array(
            'default'           =>  15,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_lheight',
            'type'            => 'select',
            'choices'         => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Btn Font Weight
    $wp_customize->add_setting('spice_popup_login_btn_fontweight',
        array(
            'default'           =>  600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-popup-login'),
        'section'         => 'spice_popup_login_typo_section',
        'setting'         => 'spice_popup_login_btn_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_popup_login_font_weight,
        'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    //Btn Font Style
    $wp_customize->add_setting('spice_popup_login_btn_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-popup-login'),
        'section'        => 'spice_popup_login_typo_section',
        'setting'        => 'spice_popup_login_btn_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_popup_login_font_style,
        'active_callback'=> 'spice_popup_login_typo_callback',
        )
    );


    //Btn Text Transform
    $wp_customize->add_setting('spice_popup_login_btn_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_transform',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    Class Spice_Popup_Login_Error_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Login Error / Success Message', 'spice-popup-login' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_popup_login_btn_error_typo',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Error_Customize_Control($wp_customize, 'spice_popup_login_btn_error_typo', 
        array(
                'active_callback'   =>  'spice_popup_login_typo_callback',
                'section'           =>  'spice_popup_login_typo_section',
                'setting'           =>  'spice_popup_login_btn_error_typo'
            )
    ));


    //Error/Success Message Family
    $wp_customize->add_setting('spice_popup_login_btn_error_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_fontfamily', 
        array(
            'label'           => esc_html__('Font Family','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_error_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_family,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );    


    //Error/Success Message Size
    $wp_customize->add_setting('spice_popup_login_btn_error_fontsize',
        array(
        'default'           =>  14,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_error_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_font_size,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Error/Success Message Height
    $wp_customize->add_setting('spice_popup_login_btn_error_lheight',
        array(
            'default'           =>  28,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_error_lheight',
            'type'            => 'select',
            'choices'         => $spice_popup_login_line_height,
            'active_callback' => 'spice_popup_login_typo_callback',
        )
    );


    //Error/Success Message Weight
    $wp_customize->add_setting('spice_popup_login_btn_error_fontweight',
        array(
            'default'           =>  600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-popup-login'),
        'section'         => 'spice_popup_login_typo_section',
        'setting'         => 'spice_popup_login_btn_error_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_popup_login_font_weight,
        'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );



    //Error/Success Message Style
    $wp_customize->add_setting('spice_popup_login_btn_error_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-popup-login'),
        'section'        => 'spice_popup_login_typo_section',
        'setting'        => 'spice_popup_login_btn_error_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_popup_login_font_style,
        'active_callback'=> 'spice_popup_login_typo_callback',
        )
    );


    //Error/Success Message Transform
    $wp_customize->add_setting('spice_popup_login_btn_error_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_popup_login_btn_error_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-popup-login'),
            'section'         => 'spice_popup_login_typo_section',
            'setting'         => 'spice_popup_login_btn_error_transform',
            'type'            => 'select',
            'choices'         =>  $spice_popup_login_text_transform,
            'active_callback' =>  'spice_popup_login_typo_callback',
        )
    );


    /*===================================================================================================================
    *   Spice Popup Login Color Setting *
    *====================================================================================================================== */
    
    $wp_customize->add_section('spice_popup_login_clr_section', 
        array(
            'title'    => esc_html__('Color Settings', 'spice-popup-login' ),
            'panel'    => 'spice_popup_login',
            'priority' => 3
        )
    );
    

    $wp_customize->add_setting('enable_spice_popup_login_clr',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_popup_login_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Toggle_Control( $wp_customize, 'enable_spice_popup_login_clr',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-popup-login'  ),
            'section'   =>  'spice_popup_login_clr_section',
            'setting'   =>  'enable_spice_popup_login_clr',
            'type'      =>  'toggle'
        )
    ));


    //Login/Logout
    $wp_customize->add_setting('spice_popup_login_logout_color', 
        array(
            'default'           => '#ffffff',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_logout_color', 
        array(
            'label'             =>  esc_html__('Login / Logout', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_logout_color'
        )
    ));

    //Login/Logout Hover
    $wp_customize->add_setting('spice_popup_login_logout_color_hover', 
        array(
            'default'           => '#ff6f61',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_logout_color_hover', 
        array(
            'label'             =>  esc_html__('Login / Logout Hover', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_logout_color_hover'
        )
    ));

    //Popup Background Overlay Color
    $wp_customize->add_setting('spice_popup_login_overlay', 
        array(
            'default'           => 'rgba(0, 0, 0, .75)',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new Spice_Popup_Login_Alpha_Color_Control($wp_customize, 'spice_popup_login_overlay', 
        array(
            'label'             =>  esc_html__('Popup Background Overlay', 'spice-popup_login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_overlay'
        )
    ));

   //Form Heading Color
    $wp_customize->add_setting('spice_popup_login_heading_color', 
        array(
            'default'           => '#000000',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_heading_color', 
        array(
            'label'             =>  esc_html__('Heading Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_heading_color'
        )
    ));

    //Form Title Color
    $wp_customize->add_setting('spice_popup_login_title_color', 
        array(
            'default'           => '#000000',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_title_color', 
        array(
            'label'             =>  esc_html__('Form Title Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_title_color'
        )
    ));

    //Label Color
    $wp_customize->add_setting('spice_popup_login_label_color', 
        array(
            'default'           => '#000000',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_label_color', 
        array(
            'label'             =>  esc_html__('Label Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_label_color'
        )
    ));

    //Link Color
    $wp_customize->add_setting('spice_popup_login_link_color', 
        array(
            'default'           => '#e25c4c',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_link_color', 
        array(
            'label'             =>  esc_html__('Link Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_link_color'
        )
    ));

    //Link Hover Color
    $wp_customize->add_setting('spice_popup_login_link_hover_color', 
        array(
            'default'           => '#e25c4c',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_link_hover_color'
        )
    ));

    //Button Text Color
    $wp_customize->add_setting('spice_popup_login_btn_txt_color', 
        array(
            'default'           => '#ffffff',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_btn_txt_color', 
        array(
            'label'             =>  esc_html__('Button Text Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_btn_txt_color'
        )
    ));

    //Button Text Hover Color
    $wp_customize->add_setting('spice_popup_login_btn_txt_hover_color', 
        array(
            'default'           => '#ffffff',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_btn_txt_hover_color', 
        array(
            'label'             =>  esc_html__('Button Hover Text Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_btn_txt_hover_color'
        )
    ));

    //Button Background Color
    $wp_customize->add_setting('spice_popup_login_btn_bg_color', 
        array(
            'default'           => '#e25c4c',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_btn_bg_color', 
        array(
            'label'             =>  esc_html__('Button Background Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_btn_bg_color'
        )
    ));

    //Button Background Hover Color
    $wp_customize->add_setting('spice_popup_login_btn_bg_hover_color', 
        array(
            'default'           => '#e25c4c',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_btn_bg_hover_color', 
        array(
            'label'             =>  esc_html__('Button Hover Background Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_btn_bg_hover_color'
        )
    ));

    //Login Error Message Color
    $wp_customize->add_setting('spice_popup_login_error_color', 
        array(
            'default'           => 'ff0000',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_error_color', 
        array(
            'label'             =>  esc_html__('Login Error Message Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_error_color'
        )
    ));

    //Success Message Color
    $wp_customize->add_setting('spice_popup_login_success_color', 
        array(
            'default'           => '#00c774',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_popup_login_success_color', 
        array(
            'label'             =>  esc_html__('Success Message Color', 'spice-popup-login' ),
            'active_callback'   =>  'spice_popup_login_color_callback',
            'section'           =>  'spice_popup_login_clr_section',
            'setting'           =>  'spice_popup_login_success_color'
        )
    ));

}
add_action( 'customize_register', 'spice_popup_login_customizer_controls' );