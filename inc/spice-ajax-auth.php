<?php
function spice_popup_ajax_auth_init(){	
	wp_register_style( 'spice-popup-ajax-auth-style', SPICE_POPUP_LOGIN_URL . 'assets/css/spice-ajax-auth-style.css' );
	wp_enqueue_style('spice-popup-ajax-auth-style');
	
	wp_register_script('spice-popup-validate-script', SPICE_POPUP_LOGIN_URL . 'assets/js/jquery.validate.js', array('jquery') ); 
    wp_enqueue_script('spice-popup-validate-script');

    wp_register_script('spice-popup-ajax-auth-script', SPICE_POPUP_LOGIN_URL . 'assets/js/spice-ajax-auth-script.js', array('jquery') ); 
    wp_enqueue_script('spice-popup-ajax-auth-script');

    //pass our parameters through to WordPress’s ajax handler file
    wp_localize_script( 'spice-popup-ajax-auth-script', 'spice_popup_ajax_auth_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_jsaction', 'spice_popup_ajax_callback' );


	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'spice_popup_ajax_register' );
}

add_action('init', 'spice_popup_ajax_auth_init');
  
function spice_popup_ajax_callback(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'spice-ajax-login-nonce', 'spice-ajax-security' );

    // Nonce is checked, get the POST data and sign user on
	auth_user_login($_POST['username'], $_POST['password'], 'Login'); 
}

function spice_popup_ajax_register(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'spice-ajax-register-nonce', 'spice-ajax-security' );
		
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
    $info['user_pass'] = sanitize_text_field($_POST['password']);
	$info['user_email'] = sanitize_email( $_POST['email']);
	
	// Register the user
    $user_register = wp_insert_user( $info );
 	if ( is_wp_error($user_register) ){	
		$error  = $user_register->get_error_codes()	;
		
		if(in_array('empty_user_login', $error))
			echo json_encode(array('spice_popup_login_arr'=>false, 'message'=>__($user_register->get_error_message('empty_user_login'))));
		elseif(in_array('existing_user_login',$error))
			echo json_encode(array('spice_popup_login_arr'=>false, 'message'=>__('This username is already registered.')));
		elseif(in_array('existing_user_email',$error))
        echo json_encode(array('spice_popup_login_arr'=>false, 'message'=>__('This email address is already registered.')));
    } else {
	  auth_user_login($info['nickname'], $info['user_pass'], 'Registration');       
    }

    die();
}

function auth_user_login($user_login, $password, $login)
{
	$info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $password;
    $info['remember'] = true;
	
	
	//Authenticates and logs a user in with ‘remember’ capability.
	$user_signon = wp_signon( $info, '' ); // From false to '' since v4.9



	//Checks whether the given variable is a WordPress Error.
    if ( is_wp_error($user_signon) )
    {
		echo json_encode(array('spice_popup_login_arr'=>false, 'message'=>__('Wrong username or password.')));
    }
    else 
    {
		wp_set_current_user($user_signon->ID); 
        echo json_encode(array('spice_popup_login_arr'=>true, 'message'=>__($login.' successful, redirecting...')));
    }
	
	die();
}