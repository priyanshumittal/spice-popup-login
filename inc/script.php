<?php if(get_theme_mod('spice_popup_login_typo',false) === true): ?>
<style type="text/css">
body .nav-item.spice-popup-login .nav-link, body .entry-content a.spice-popup-login-a
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_fontfamily','Poppins'));?>' !important;
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_fontsize',14));?>px  !important;
    line-height: <?php echo esc_attr(get_theme_mod('spice_popup_login_lheight','inherit'));?><?php if(get_theme_mod('spice_popup_login_lheight','inherit')!='inherit'):?>px<?php endif;?> !important;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_fontweight',600));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_transform','default'));?>;
}
body #spice_popup_login_id .spice-popup-login-form h3
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_headings_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_headings_fontsize',18));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_headings_lheight',18));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_headings_fontweight',500));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_headings_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_headings_transform','default'));?>;
}
body #spice_popup_login_id .spice-popup-login-form h2
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_title_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_title_fontsize',27));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_title_lheight',30));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_title_fontweight',600));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_title_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_title_transform','default'));?>;
}
body .spice-popup-login-form label
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_label_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_label_fontsize',14));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_label_lheight',22));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_label_fontweight',400));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_label_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_label_transform','default'));?>;
}
body .spice-popup-login-form .spice-popup-lostpassword
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_txt_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_txt_fontsize',11));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_txt_lheight',18));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_txt_fontweight',400));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_txt_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_txt_transform','default'));?>;
}
body .spice-popup-login-form .spice_popup_submit_button
{
	font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_btn_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_btn_fontsize',13));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_btn_lheight',15));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_btn_fontweight',600));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_transform','default'));?>;
}
body .spice-popup-login-form .status
{
    font-family:'<?php echo esc_attr(get_theme_mod('spice_popup_login_btn_error_fontfamily','Poppins'));?>';
    font-size: <?php echo intval(get_theme_mod('spice_popup_login_btn_error_fontsize',14));?>px;
    line-height: <?php echo intval(get_theme_mod('spice_popup_login_btn_error_lheight',28));?>px;
    font-weight: <?php echo intval(get_theme_mod('spice_popup_login_btn_error_fontweight',600));?>;
    font-style: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_error_fontstyle','normal'));?>;
    text-transform: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_error_transform','default'));?>;
}
</style>
<?php endif;
if(get_theme_mod('enable_spice_popup_login_clr',false)===true): ?>
<style type="text/css">
body .parent-menu.nav-item.menu-item.spice-popup-login a
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_logout_color','#ffffff'));?>;
} 
body .parent-menu.nav-item.menu-item.spice-popup-login a:hover
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_logout_color_hover','#ff6f61'));?>;
}   
body .login_overlay
{
    background-color: <?php echo esc_attr(get_theme_mod('spice_popup_login_overlay','rgba(0, 0, 0, .75)'));?>;
}
body #spice_popup_login_id .spice-popup-login-form h3
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_heading_color','#000000'));?>; 
}
body #spice_popup_login_id .spice-popup-login-form h2
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_title_color','#000000'));?>; 
}
body .spice-popup-login-form label
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_label_color','#000000'));?>; 
}

body #spice_popup_login_id .spice-popup-login-form h3 a ,body .spice-popup-login-form a.spice-popup-lostpassword
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_link_color','#e25c4c'));?>; 
}
body #spice_popup_login_id .spice-popup-login-form h3 a:hover ,body .spice-popup-login-form a.spice-popup-lostpassword:hover
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_link_hover_color','#e25c4c'));?>; 
}
body .spice-popup-login-form .spice_popup_submit_button
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_txt_color','#ffffff'));?>; 
}
body .spice-popup-login-form .spice_popup_submit_button:hover
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_txt_hover_color','#ffffff'));?>; 
}
body .spice-popup-login-form input.spice_popup_submit_button
{
    background-color: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_color','#e25c4c'));?>;
    border: 1px solid <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_color','#e25c4c'));?>; 
    box-shadow: 0 1px 0 <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_color','#e25c4c'));?> inset;
}
body .spice-popup-login-form input.spice_popup_submit_button:hover,body .spice-popup-login-form input.spice_popup_submit_button:focus
{
    background-color: <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_hover_color','#e25c4c'));?>;
    border: 1px solid <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_hover_color','#e25c4c'));?>; 
    box-shadow: 0 1px 0 <?php echo esc_attr(get_theme_mod('spice_popup_login_btn_bg_hover_color','#e25c4c'));?> inset;
}
body .spice-popup-login-form #statuserror
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_error_color','#ff0000'));?>; 
}
body .spice-popup-login-form #statussuccess
{
    color: <?php echo esc_attr(get_theme_mod('spice_popup_login_success_color','#00c774'));?>; 
}
</style>
<?php endif;?>