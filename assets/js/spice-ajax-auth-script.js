jQuery(document).ready(function ($) {
    // Display form from link inside a popup
    $('#spice_login, #spice_signup').on('click', function (e) {
        formToFadeOut = $('form#spice_popup_register');
        formtoFadeIn = $('form#spice_popup_login');
        if ($(this).attr('id') == 'spice_signup') {
            formToFadeOut = $('form#spice_popup_login');
            formtoFadeIn = $('form#spice_popup_register');
        }
        formToFadeOut.fadeOut(500, function () {
            formtoFadeIn.fadeIn();
        })
        return false;
    });

    // Close popup
    $(document).on('click', '.login_overlay, .close', function () {
        $('form#spice_popup_login, form#spice_popup_register').fadeOut(500, function () {
            $('.login_overlay').remove();
        });
        return false;
    });

    // Show the login/signup popup on click
    $('#show_login, #show_signup').on('click', function (e) {
        $('body').prepend('<div class="login_overlay"></div>');
        if ($(this).attr('id') == 'show_login') 
            $('form#spice_popup_login').fadeIn(500);
        else 
            $('form#spice_popup_register').fadeIn(500);
        e.preventDefault();
    });

    // Perform AJAX login/register on form submit
    $('form#spice_popup_login, form#spice_popup_register').on('submit', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(spice_popup_ajax_auth_object.loadingmessage);
        action = 'jsaction';
        username =  $('form#spice_popup_login #username').val();
        password = $('form#spice_popup_login #password').val();
        email = '';
        security = $('form#spice_popup_login #spice-ajax-security').val();
        if ($(this).attr('id') == 'spice_popup_register') {
            action = 'ajaxregister';
            username = $('#signonname').val();
            password = $('#signonpassword').val();
            email = $('#email').val();
            security = $('#spice-ajax-reg-security').val();  
        }  
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: spice_popup_ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'password': password,
                'email': email,
                'spice-ajax-security': security
            },
            success: function (data) {
                if((data.message=="Wrong username or password.") || (data.message=="This username is already registered.") || (data.message=="This email address is already registered."))
                {
                    $('p.status').prop('id', 'statuserror');
                    $('p.status', ctrl).text(data.message);

                }
                else
                {
                  $('p.status').prop('id', 'statussuccess');  
                 $('p.status', ctrl).text(data.message);    
                }
                if (data.spice_popup_login_arr == true) {
                    document.location.href = spice_popup_ajax_auth_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });
    
    // Client side form validation
   if (jQuery("#spice_popup_register").length) 
        jQuery("#spice_popup_register").validate(
        { 
            rules:{
            password2:{ equalTo:'#signonpassword' 
            }   
        }}
        );
    else if (jQuery("#spice_popup_login").length) 
        jQuery("#spice_popup_login").validate();
});