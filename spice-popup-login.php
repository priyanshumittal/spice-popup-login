<?php
/*
* Plugin Name:			Spice Popup Login
* Plugin URI:			https://olivewp.org/
* Description:			This plugin offers popup forms for login, register and lost password utilities from where you want it to popup.
* Version:				1.0.1
* Requires at least:	5.3
* Requires PHP:			5.2
* Tested up to:			6.7.1
* Author:				Spicethemes
* Author URI:			https://spicethemes.com
* License:				GPLv2 or later
* License URI:			https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain:			spice-popup-login
* Domain Path:			/languages
*/

//Freemius SDK Snippet
if ( ! function_exists( 'spl_fs' ) ) {
    // Create a helper function for easy SDK access.
    function spl_fs() {
        global $spl_fs;

        if ( ! isset( $spl_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }
          
            $spl_fs = fs_dynamic_init( array(
                'id'                  => '10579',
                'slug'                => 'spice-popup-login',
                'premium_slug'        => 'spice-popup-login',
                'type'                => 'plugin',
                'public_key'          => 'pk_9921cccbf237a475d17add6d50b32',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }

        return $spl_fs;
    }
	spl_fs();
}

// Exit if accessed directly
if( ! defined('ABSPATH'))
{
	die('Do not open this file directly.');
}

/**
 * Main Spice_Popup_Login Class
 *
 * @class Spice_Popup_Login
 * @since 0.1
 * @package Spice_Popup_Login
*/

final class Spice_Popup_Login {

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   0.1
	 */
	public $version;
	private string $plugin_url;
    private string $plugin_path;

	/**
	 * Constructor function.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function __construct()
	{
		$this->plugin_url  = plugin_dir_url( __FILE__ );
		$this->plugin_path = plugin_dir_path( __FILE__ );
		$this->version     = '0.1';

		define( 'SPICE_POPUP_LOGIN_URL',     $this->plugin_url );
		define( 'SPICE_POPUP_LOGIN_PATH',    $this->plugin_path );
		define( 'SPICE_POPUP_LOGIN_VERSION', $this->version );
		add_action( 'customize_register', array( $this, 'side_popup_login_controls' ) );
		add_action( 'after_setup_theme' , array( $this, 'side_popup_login_register_options' ) );
		add_action( 'admin_enqueue_scripts', array( $this,'side_popup_login_admin_script' ));
		add_shortcode( 'spice_login', array( $this, 'side_popup_login_shortcode' ) );
		add_shortcode( 'spice_register', array( $this, 'side_popup_reg_shortcode' ) );
		add_filter( 'wp_nav_menu_items', array( $this,'side_popup_login_nav' ), 1, 3);
		add_action( 'wp_footer', array( $this, 'side_popup_login_form' ) );
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		require( SPICE_POPUP_LOGIN_PATH . 'inc/spice-ajax-auth.php' );
	}
    

	/**
	* Adds custom controls
	*/
	public function side_popup_login_controls( $wp_customize )
	{
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/customizer/controls/toggle/class-toggle-control.php' );
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/customizer/controls/color/color-control.php' );

		$wp_customize->register_control_type('Spice_Popup_Login_Toggle_Control');
	}


  	/**
	* Shortcode Login/ Option
	*/
	public function side_popup_login_shortcode($atts){
		extract(shortcode_atts( array(
  		'title' => 'Login',
  		'logout' => 'Logout',
		), $atts ));
	    	$items='';
	    	if (is_user_logged_in())
	    	{
	    		$items .= '<a class="spice-popup-login-a" href="'. wp_logout_url( home_url() ) .'">' . $logout . '</a>';
	    	}
	    	else
	    	{
	    		$items .= '<a class="spice-popup-login-a" id="show_login" href="'. esc_url( admin_url() ) .'">' . $title . '</a>';	
	    	}
	    
	    return $items;

	}

	/**
	* Shortcode Logout Option
	*/
	public function side_popup_reg_shortcode($atts){
		extract(shortcode_atts( array(
  		'title' => 'Register',
  		'logout' => 'Logout',
		), $atts ));
	    	$items='';
	    	if (is_user_logged_in())
	    	{
	    		$items .= '<a class="spice-popup-login-a" href="'. wp_logout_url( home_url() ) .'">' . $logout . '</a>';
	    	}
	    	else
	    	{
	    		$items .= '<a class="spice-popup-login-a" id="show_signup" href="'. esc_url( admin_url() ) .'">' . $title . '</a>';	
	    	}
	    
	    return $items;

	}


	/**
	* In Menu Login/Logout Option
	*/
	public function side_popup_login_nav($items, $args){
	    if( $args->theme_location == 'primary' || $args->theme_location != 'primary' )
	    {
	    	if (is_user_logged_in()) 
	    	{
	    		$items .= '<li class="parent-menu nav-item menu-item spice-popup-login"><a class="nav-link" href="'. wp_logout_url( home_url() ) .'"><span class="menu-text">' . __( get_theme_mod('spice_popup_login_logout_text','Logout') ) . '</span></a></li>';
	    	}
	    	else
	    	{
	    		$items .= '<li class="parent-menu nav-item menu-item spice-popup-login"><a class="nav-link" id="show_login" href><span class="menu-text">' . __( get_theme_mod('spice_popup_login_text','Sign in / Join') ) .'</span></a></li>';
	    	}
	    }
	    return $items;

	}


	/**
	* Login/Register Form
	*/
	public function side_popup_login_form()
	{
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/spice-form.php' );
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/script.php' );
	}


	/**
	* Adds customizer options
	*/
	public function side_popup_login_register_options()
	{
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/customizer/sanitization.php' );
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/customizer/customizer.php' );
		require_once ( SPICE_POPUP_LOGIN_PATH . 'inc/customizer/fonts.php' );
	}


	/**
	* Load admin style
	*/
	public function side_popup_login_admin_script()
	{
		wp_enqueue_style('side-popup-login-admin', SPICE_POPUP_LOGIN_URL .'assets/css/admin.css');
	}


 	/**
	 * Load the localisation file.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'spice-side-panel' , false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}



}

new Spice_Popup_Login;