=== Spice Popup Login ===

Contributors:           spicethemes
Tags:                   login, popup login, register, popup register, forget password
Requires at least:      5.3
Requires PHP:           5.2
Tested up to:           6.7.1
Stable tag:             1.0.1
License:                GPLv2 or later
License URI:            https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin offers popup forms for login, register and lost password utilities from where you want it to popup.

<h3>Key Features</h3>
* Login, Popup Login, Register, Popup Register, Forget Password.
* Typography setting
* Color setting

== Changelog ==

@Version 1.0.1
* Updated freemius directory and fixed warning issue.

@Version 1.0
* Updated freemius directory.

@Version 0.2.1
* Updated Freemius Code

@Version 0.1
* Initial release

======= External Resources =======

jQuery Validation
Copyright: (c) 2014 Jörn Zaefferer
License: MIT License
Source: http://jqueryvalidation.org/